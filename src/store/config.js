import dayjs from 'dayjs'

import { $HTTP } from '@/utils/axios'
import { helpers } from '@/utils/helpers'

const data = {
  statusList: [],
  languageList: [],
  expirationDate: ''
}

export const state = {
  data
}

export const getters = {
  getData: (state) => {
    return state.data
  }
}

export const mutations = {
  UPDATE_DATA(state, payload) {
    state.data = { ...state.data, ...payload }
  }
}

export const actions = {
  setData: (context, payload) => {
    context.commit('UPDATE_DATA', payload)
  },
  resetData: (context) => {
    context.commit('UPDATE_DATA', data)
  },
  fetchData: async (context, payload) => {
    const expirationDate = context.state.data.expirationDate
    const currentDate = dayjs().valueOf()

    let isExpired = currentDate > expirationDate
    if (payload?.force) {
      isExpired = true
    }

    if (expirationDate == '' || isExpired) {
      //helpers.showLoader('Getting configurations...')

      try {
        const res = await $HTTP.get('/admin/config')

        if (res && res.status === 200 && res.data && res.data.data) {
          //helpers.hideLoader()

          const configInfo = res.data.data
          configInfo.expirationDate = dayjs()
            .add(1, 'hour')
            .valueOf()

          context.commit('UPDATE_DATA', configInfo)
          return
        }

        throw new Error('Server not responding, Please try again later.')
      } catch (err) {
        //helpers.hideLoader()
        //helpers.catchError(err, true)
      }
    } else {
      return
    }
  }
}

export const config = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
