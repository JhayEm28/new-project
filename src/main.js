import { createApp } from 'vue'

import Framework7 from 'framework7/lite-bundle'
import Framework7Vue, { registerComponents } from 'framework7-vue/bundle'

import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import 'element-plus/lib/theme-chalk/display.css'

import 'framework7/framework7-bundle.min.css'
import '@/assets/css/main.css'
import '@/assets/fonts/style.css'

import utils from '@/utils'
import App from '@/App.vue'

Framework7.use(Framework7Vue)

const mainApp = createApp(App)
mainApp.use(ElementPlus)

registerComponents(mainApp)
utils(mainApp)

// REGISTER FONT AWESOME COMPONENTS
import { FontAwesomeIcon } from '@/fontawesome.js'
mainApp.component('FontAwesomeIcon', FontAwesomeIcon)

mainApp.mount('#app')
