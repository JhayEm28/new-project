const routes = [
  {
    name: '',
    path: '/',
    redirect: '/home/'
  },
  {
    name: 'home',
    path: '/home/',
    asyncComponent: () => import('@/pages/HomePage.vue')
  },
  {
    name: 'productlist',
    path: '/list/',
    asyncComponent: () => import('@/pages/ProductsPage.vue')
  },
  {
    name: 'splash',
    path: '/splash/',
    asyncComponent: () => import('@/pages/SplashPage.vue')
  },
  {
    name: 'marketplace',
    path: '/marketplace/',
    asyncComponent: () => import('@/pages/MarketPlacePage.vue')
  },
  {
    name: 'agreement',
    path: '/agreement/',
    asyncComponent: () => import('@/pages/UserAgreementPage.vue')
  },
  {
    name: 'contact',
    path: '/contact/',
    asyncComponent: () => import('@/pages/ContactPage.vue')
  },
  {
    path: '(.*)',
    asyncComponent: () => import('@/pages/NotFoundPage.vue')
  }
]

export default routes
