import configs from '@/configs.js'

const install = (app) => {
  app.config.globalProperties.$configs = configs
}

export { install as default, configs }
