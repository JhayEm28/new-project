let configs = {
  baseURL: '',
  environment: '',
  baseURLInfo: {
    local: 'http://localhost:3000/api',
    development: 'http://128.199.82.172:3000/api',
    production: 'http://localhost:3000/api'
  },
  axiosTimeout: 300000,
  tokenSession: 'jjx-frontend',
  tokenExpiration: 86400000,
  tokenExpirationRememberMe: 2592000000,
  encryptionSecret: 'z1123Hs42as2kadhs1$23@Sasd9kjaj2xjs9131aki11asdjd'
}

configs.environment = 'development'
configs.baseURL = configs.baseURLInfo[configs.environment]

export default configs
